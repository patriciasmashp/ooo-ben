const Foo = { template: "<div>foo</div>" }
const Bar = { template: "<div>bar</div>" }

const routes = [
    { path: '/foo', component: Foo },
    { path: '/bar', component: Bar },
    // { path: '/bar', component: 'button-counter' }
]

const router = new VueRouter({
    routes,
    mode: 'history',
})


const app = new Vue({
    router
}).$mount('#app')