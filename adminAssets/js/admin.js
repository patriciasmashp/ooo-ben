console.log("admin js")


div = $('div#test')[0]

// document.addEventListener('dragover', ev => ev.preventDefault())
// document.addEventListener('drop', ev => ev.preventDefault())
// Dropzone.autoDiscover = false;
files = []
mainImg = undefined
$('#summernote').summernote({
    height: 350, // set editor height
    minHeight: null, // set minimum height of editor
    maxHeight: null, // set maximum height of editor
    focus: false // set focus to editable area after initializing summernote
});
var GETparams = window
    .location
    .search
    .replace('?', '')
    .split('&')
    .reduce(
        function(p, e) {
            var a = e.split('=');
            p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        }, {}
    );
if (GETparams["type"] == 'project') {


    var form = document.querySelector('form#adminForm')

    Dropzone.options.uploadMain = {
        url: "/projects/add",
        resizeMethod: "crop",
        addRemoveLinks: true,
        paramName: 'imgMain',
        autoProcessQueue: false,
        renameFile: function(file) {
            ext = file.name.split('.')[1]
                // file.name =
                // file.upload.name = 'main.' + ext
            f = file
            return 'main.' + ext


        },
        accept: function(file, done) {
            if (this.files.length > 1) {
                this.removeFile(file)
                done("only One")
            } else {

                done()
            }
        },
        init: function() {
            mainUpload = this
            this.on('addedfile', function(e) {
                if (!mainUpload.files.length == 0) {
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "none"
                }
            })
            this.on('removedfile', function(e) {

                if (mainUpload.files.length == 0) {
                    console.log(this)
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "block"
                }
            })

        },
        previewTemplate: document
            .querySelector('#tpl')
            .innerHTML
    }
    Dropzone.options.upload = {
        url: "/projects/add",
        resizeMethod: "crop",
        addRemoveLinks: true,
        maxFilesize: 500,
        autoProcessQueue: false,
        init: function() {
            this.on('addedfile', function(e) {
                if (!detailDropzone.files.length == 0) {
                    message = document.querySelector("div#dz-message")
                    message.style.display = "none"
                }
            })
            this.on('removedfile', function(e) {

                if (mainUpload.files.length == 0) {
                    console.log(this)
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "block"
                }
            })
            detailDropzone = this;
        },
        previewTemplate: document
            .querySelector('#tpl')
            .innerHTML
    }

}
if (GETparams["type"] == 'post') {


    $('.inline-editor').summernote({
        airMode: true
    });

    window.edit = function() {
        $(".click2edit").summernote()
    }
    window.save = function() {
        $(".click2edit").destroy()
    }

    Dropzone.options.uploadMain = {
        url: "/blog/add",
        resizeMethod: "crop",
        addRemoveLinks: true,
        paramName: 'imgMain',
        autoProcessQueue: false,
        renameFile: function(file) {
            ext = file.name.split('.')[1]
                // file.name =
                // file.upload.name = 'main.' + ext
            f = file
            return 'main.' + ext


        },
        accept: function(file, done) {
            if (this.files.length > 1) {
                this.removeFile(file)
                done("only One")
            } else {

                done()
            }
        },
        init: function() {
            mainUpload = this
            this.on('addedfile', function(e) {
                if (!mainUpload.files.length == 0) {
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "none"
                }
            })
            this.on('removedfile', function(e) {

                if (mainUpload.files.length == 0) {
                    console.log(this)
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "block"
                }
            })

        },
        previewTemplate: document
            .querySelector('#tpl')
            .innerHTML

    }
}
if (GETparams["type"] == 'docs') {

    $('#project-id, #posts-id').multiSelect();
    $('#select-all-projects').click(function() {
        $('#project-id').multiSelect('select_all');
        return false;
    });
    $('#deselect-all-projects').click(function() {
        $('#project-id').multiSelect('deselect_all');
        return false;
    });
    $('#select-all-posts').click(function() {
        $('#posts-id').multiSelect('select_all');
        return false;
    });
    $('#deselect-all-posts').click(function() {
        $('#posts-id').multiSelect('deselect_all');
        return false;
    });

    mainDropzone = Dropzone.options.docsUpload = {
        url: "/docs/add",
        maxFiles: 1,
        autoProcessQueue: false,
        init: function() {
            this.on("addedfile", function() {
                if (this.files[1] != null) {
                    this.removeFile(this.files[0]);
                }
            });
            mainUpload = this
            this.on('addedfile', function(e) {
                if (!mainUpload.files.length == 0) {
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "none"
                }
            })
            this.on('removedfile', function(e) {

                if (mainUpload.files.length == 0) {
                    console.log(this)
                    message = document.querySelector("div#dz-message-main")
                    message.style.display = "block"
                }
            })
        },
        previewTemplate: document
            .querySelector('#tpl')
            .innerHTML
    }

}

function readFormData(data) {
    for (key of data.keys()) {
        console.log(`${key}: ${data.get(key)}`);
    }

}
var submitButton = document.querySelector("#btnUpload")
submitButton.addEventListener("click", function(e) {

    const form = document.querySelector("form#adminForm")
    formData = new FormData(form)
    let name

    e.preventDefault()


    if (typeof detailDropzone !== 'undefined') {
        for (let i = 0; i < detailDropzone.files.length; i++) {
            const element = detailDropzone.files[i];
            formData.append('imges[]', element)
        }

    }


    if (GETparams["type"] == 'docs') {
        formData.append('file', mainUpload.files[0])
        name = document.querySelector("#docs-name").value
    } else {
        name = form.name.value
        formData.append('mainImg', mainUpload.files[0])
    }




    if (!name) {
        document.querySelector("#docs-name").parentElement.classList.add("has-error")
        const tabLinkInfo = document.querySelector('a[href="#home"]')
        const tabLinkHome = document.querySelector('a[href="#settings"]')
        const tabSettings = document.querySelector('div#settings')
        const tabHome = document.querySelector('div#home')
        if (!tabLinkInfo) {
            return
        }
        tabLinkInfo.classList.add('active')
        tabLinkHome.classList.remove('active')
        tabSettings.classList.add('active')
        tabHome.classList.remove('active')


        return
    }
    readFormData(formData)
    switch (GETparams["type"]) {
        case 'post':
            urlParam = '/blog/add'
            break;
        case 'project':
            urlParam = '/projects/add'
            break;
        case 'docs':
            urlParam = '/document/add'
            break;


    }

    let promise = fetch(urlParam, {
        body: formData,
        method: "post",
    })

    promise.then(
        response => {
            if (response.status == 200) {
                $.toast({
                    heading: 'Запись добавлена',
                    text: '',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
                if (GETparams["type"] == 'project') {
                    detailDropzone.removeAllFiles()
                }
                if (mainUpload) {

                    mainUpload.removeAllFiles()
                }
                form.reset()
                $('#summernote').summernote('reset');
                $('#project-id').multiSelect('deselect_all');
                $('#posts-id').multiSelect('deselect_all');
                formData = 0
            }
            return response.text() // объект класса Response
        }
    ).then(text => {
        console.log(text) // В переменную text попадет текст страницы
    })
    if (GETparams["type"] == 'project') {
        // detailDropzone.processQueue();
    }
    // Dropzone.processQueue();
});