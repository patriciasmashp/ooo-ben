import './index.html'
import 'jquery'
import '@popperjs/core'
import "jquery.easing"
// import "jquery-migrate"
import "../node_modules/jquery-waypoints/waypoints.js"
import "../node_modules/appear/dist/appear"
import "jquery.animate-number"
import "scrollax"



import 'animate.css'
import "../node_modules/font-awesome/css/font-awesome.css"
import "owl.carousel"
import "./css/owl.theme.default.min.css"
import "magnific-popup"
import "bootstrap-datepicker"
import "@renekorss/jquery-timepicker"
import "./css/flaticon.css"
import "./css/style.css"
import "./css/animFrom.css"


import "./js/main"
import "./js/app.js"