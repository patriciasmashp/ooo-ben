(function($) {


    const hero = document.querySelector("section.hero-wrap")
        // new appear({
        //     elements: function elements() {
        //         // work with all elements with the class "track"
        //         return document.getElementsByClassName('ftco-animate');
        //     },
        //     appear: function appear(el) {
        //         el.animate([{ // from
        //                 opacity: 0,
        //                 visibility: 0,
        //             },
        //             { // to
        //                 visibility: 1,
        //                 opacity: 1,
        //                 // transform: none
        //             }
        //         ], 500);
        //         // el.style.opacity = 1

    //     },


    // })



    var fullHeight = function() {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function() {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    // loader
    $(window).on('load', function() {
        // $('#ftco-loader').fadeOut().end().delay(400).fadeOut('slow');
    });
    // var loader = function() {
    //     setTimeout(function() {
    //         console.log($('#ftco-loader').length)
    //         if ($('#ftco-loader').length > 0) {
    //             $('#ftco-loader').removeClass('show');
    //         }
    //     }, 1);
    // };
    // loader();

    // Scrollax
    $.Scrollax();


    var carousel = function() {
        $('.carousel-testimony').owlCarousel({
            autoplay: true,
            autoHeight: true,
            center: true,
            loop: true,
            items: 1,
            margin: 30,
            stagePadding: 0,
            nav: false,
            dots: true,
            navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });

    };
    carousel();

    $('nav .dropdown').hover(function() {
        var $this = $(this);
        // 	 timer;
        // clearTimeout(timer);
        $this.addClass('show');
        $this.find('> a').attr('aria-expanded', true);
        // $this.find('.dropdown-menu').addClass('animated-fast fadeInUp show');
        $this.find('.dropdown-menu').addClass('show');
    }, function() {
        var $this = $(this);
        // timer;
        // timer = setTimeout(function(){
        $this.removeClass('show');
        $this.find('> a').attr('aria-expanded', false);
        // $this.find('.dropdown-menu').removeClass('animated-fast fadeInUp show');
        $this.find('.dropdown-menu').removeClass('show');
        // }, 100);
    });


    $('#dropdown04').on('show.bs.dropdown', function() {
        console.log('show');
    });

    // scroll
    var scrollWindow = function() {
        $(window).scroll(function() {
            var $w = $(this),
                st = $w.scrollTop(),
                navbar = $('.ftco_navbar'),
                sd = $('.js-scroll-wrap');

            if (st > 150) {
                if (!navbar.hasClass('scrolled')) {
                    navbar.addClass('scrolled');
                }
            }
            if (st < 150) {
                if (navbar.hasClass('scrolled')) {
                    navbar.removeClass('scrolled sleep');
                }
            }
            if (st > 350) {
                if (!navbar.hasClass('awake')) {
                    navbar.addClass('awake');
                }

                if (sd.length > 0) {
                    sd.addClass('sleep');
                }
            }
            if (st < 350) {
                if (navbar.hasClass('awake')) {
                    navbar.removeClass('awake');
                    navbar.addClass('sleep');
                }
                if (sd.length > 0) {
                    sd.removeClass('sleep');
                }
            }
        });
    };
    scrollWindow();



    // magnific popup
    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });

    $('.appointment_date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });
    $('.appointment_time').timepicker();

    // new appear({
    //     elements: function elements() {
    //         // work with all elements with the class "track"
    //         return document.getElementsByClassName("ftco-animate");
    //     },
    //     appear: function appear(el) {
    //         el.animate(
    //             [{
    //                     opacity: 0,
    //                     transform: "translate3d(0, 40px, 0)",
    //                 },
    //                 {
    //                     opacity: 1,
    //                     transform: "none",
    //                 },
    //             ],
    //             700
    //         );
    //         el.style.opacity = 1;
    //     },
    // });
})(jQuery);